﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using rnd = EquimarFac.FelWebService;

namespace EquimarFac.DAO
{
    class FelWebServiceDAO
    {
        public List<string> conceptos, infoaduanera;
        //credenciales

        public string emisorRFC;

        public string CuentaFEL;

        public string PasswordFEL;
        //Datos emisor
        public string NombreEmisor { get; set; }


        public int RegimenFiscalEmisor { get; set; }

        // Datos Receptor
        public string NombreCliente { get; set; }

        public string Contacto { get; set; }

        public int RegimenFiscalReceptor { get; set; }

        public string Telefono { get; set; }

        public string Email { get; set; }

        public string rfcReceptor { get; set; }

        public string nombreReceptor { get; set; }

        public string calleReceptor { get; set; }

        public string noExteriorReceptor { get; set; }

        public string noInteriorReceptor { get; set; }

        public string coloniaReceptor { get; set; }

        public string localidadReceptor { get; set; }

        public string referenciaReceptor { get; set; }

        public string municipioReceptor { get; set; }

        public string estadoReceptor { get; set; }

        public string paisReceptor { get; set; }


        public string codigoPostalReceptor { get; set; }

        public string UsoCFDIReceptor { get; set; }

        //Datos CFDI
        public string ClaveCFDI { get; set; }

        public string formaDePago { get; set; }

        public string parcialidades { get; set; }

        public string condicionesDePago { get; set; }

        public string metodoDePago { get; set; }

        public string descuento { get; set; }

        public string porcentajeDescuento { get; set; }

        public string motivoDescuento { get; set; }

        public string moneda { get; set; }

        public string tipoCambio { get; set; }

        public string fechaTipoCambio { get; set; }

        public string totalImpuestosRetenidos { get; set; }

        public string totalImpuestosTrasladados { get; set; }

        public string subTotal { get; set; }

        public string total { get; set; }

        public string importeConLetra { get; set; }

        public string LugarExpedicion { get; set; }

        public string NumCuentaPago { get; set; }

        public string FolioFiscalOrig { get; set; }

        public string SerieFolioFiscalOrig { get; set; }

        public string FechaFolioFiscalOrig { get; set; }

        public string MontoFolioFiscalOrig { get; set; }
        //Datos Varios
        public string datosEtiquetas1 { get; set; }
        public string datosEtiquetas2 { get; set; }
        public string datosEtiquetas3 { get; set; }
        public string datosEtiquetas4 { get; set; }
        public string datosEtiquetas5 { get; set; }
        public string datosEtiquetas6 { get; set; }
        public string datosConceptos { get; set; }
        public string datosInfoAduanera { get; set; }
        public string datosRetenidosIVA { get; set; }
        public string datosRetenidosISR { get; set; }
        public string datosTraslados1 { get; set; }
        public string datosRetenidosLocales1 { get; set; }
        public string datosRetenidosLocales2 { get; set; }
        public string datosTrasladosLocales1 { get; set; }
        public string UUID { get; set; }
        public string UUIDRelacionado { get; set; }
        public string[] AUUID { get; set; }
        public string usoCFDI { get; set; }


        public string[] GenerarCDFI()
        {
            string[] datosInfoAduanera = new string[infoaduanera.Count];
            string[] datosRetenidos = new string[1];
            string[] datosTraslados = new string[1];
            string[] datosRetenidosLocales = new string[1];
            string[] datosTrasladosLocales = new string[1];
            string[] respuesta = new string[6];
EquimarFac.ServicioFel.ConexionRemotaClient ConexionRemota = new EquimarFac.ServicioFel.ConexionRemotaClient();
            EquimarFac.ServicioFel.Credenciales datosUsuario = new ServicioFel.Credenciales();
            EquimarFac.ServicioFel.Comprobante33R datosCFDI = new ServicioFel.Comprobante33R();
            datosCFDI.Receptor = new ServicioFel.ReceptorR();
            List<EquimarFac.ServicioFel.ConceptoR> listaconceptos = new List<ServicioFel.ConceptoR>();
            
            EquimarFac.ServicioFel.AddendaCFDR addenda = new ServicioFel.AddendaCFDR();
List<EquimarFac.ServicioFel.EtiquetaPersonalizadaR> listaEtiquetasPersonalizadas = new List<ServicioFel.EtiquetaPersonalizadaR>();
EquimarFac.ServicioFel.ConexionRemotaClient coneccion = new ServicioFel.ConexionRemotaClient();
EquimarFac.ServicioFel.RespuestaOperacionCR respuesta1 = new ServicioFel.RespuestaOperacionCR();
            try
            {

                datosUsuario.Cuenta = CuentaFEL;
            
            datosUsuario.Usuario = emisorRFC;
            
            datosUsuario.Password = PasswordFEL;

            datosCFDI.Receptor = new ServicioFel.ReceptorR();
            datosCFDI.Receptor.Nombre = NombreCliente;
            datosCFDI.Receptor.Rfc = rfcReceptor;
            datosCFDI.Receptor.UsoCFDI = usoCFDI;
            datosCFDI.Emisor = new ServicioFel.EmisorR();

            datosCFDI.Emisor.Nombre = "Equimar Progreso, SA. de CV.";
            datosCFDI.Emisor.RegimenFiscal = RegimenFiscalEmisor.ToString();
            datosCFDI.ClaveCFDI = ClaveCFDI;
            if (ClaveCFDI == "CRE")
            {
                ServicioFel.CfdiRelacionadoR cfdir = new ServicioFel.CfdiRelacionadoR();
                List<EquimarFac.ServicioFel.CfdiRelacionadoR> cfdirelacionado = new List<ServicioFel.CfdiRelacionadoR>();



                ServicioFel.CfdiRelacionadosR cfdirelacionados = new ServicioFel.CfdiRelacionadosR();

                cfdir.UUID = UUIDRelacionado;

                cfdirelacionado.Add(cfdir);

                cfdirelacionados.CfdiRelacionado = cfdirelacionado.ToArray();
                cfdirelacionados.TipoRelacion = "01";
                datosCFDI.CfdiRelacionados = cfdirelacionados;

            }
            datosCFDI.FormaPago = metodoDePago;
            datosCFDI.MetodoPago = formaDePago;
            if (descuento == "")
            {
                //datosCFDI.Descuento = 0;
            }
            else
            {
                datosCFDI.Descuento = Convert.ToDecimal(descuento);
            }

            datosCFDI.LugarExpedicion = "97320";
            datosCFDI.Moneda = moneda;
            if ((tipoCambio == "") | (tipoCambio == "0,00"))
            {
                //datosCFDI.TipoCambio = "";
            }
            else
            {
                datosCFDI.TipoCambio = Convert.ToDecimal(tipoCambio);
            }
            datosCFDI.SubTotal = Convert.ToDecimal(subTotal);
            datosCFDI.Total = Convert.ToDecimal(total);
            datosCFDI.Folio = FolioFiscalOrig;
            if (datosEtiquetas1 != null)
            {
                string[] r = new string[4];
                char delimiter = '|';
                r = datosEtiquetas1.Split(delimiter);
                EquimarFac.ServicioFel.EtiquetaPersonalizadaR Etiquetas = new EquimarFac.ServicioFel.EtiquetaPersonalizadaR();
                Etiquetas.Nombre = r[1];
                Etiquetas.Valor = r[2];
                listaEtiquetasPersonalizadas.Add(Etiquetas);
            }
            if (datosEtiquetas2 != null)
            {
                string[] r = new string[4];
                r = datosEtiquetas2.Split('|');
                EquimarFac.ServicioFel.EtiquetaPersonalizadaR Etiquetas = new EquimarFac.ServicioFel.EtiquetaPersonalizadaR();
                Etiquetas.Nombre = r[1];
                Etiquetas.Valor = r[2];
                listaEtiquetasPersonalizadas.Add(Etiquetas);
            }
            if (datosEtiquetas3 != null)
            {
                string[] r = new string[4];
                r = datosEtiquetas3.Split('|');
                EquimarFac.ServicioFel.EtiquetaPersonalizadaR Etiquetas = new EquimarFac.ServicioFel.EtiquetaPersonalizadaR();
                Etiquetas.Nombre = r[1];
                Etiquetas.Valor = r[2];
                listaEtiquetasPersonalizadas.Add(Etiquetas);
            }
            if (datosEtiquetas4 != null)
            {
                string[] r = new string[4];
                r = datosEtiquetas4.Split('|');
                EquimarFac.ServicioFel.EtiquetaPersonalizadaR Etiquetas = new EquimarFac.ServicioFel.EtiquetaPersonalizadaR();
                Etiquetas.Nombre = r[1];
                Etiquetas.Valor = r[2];
                listaEtiquetasPersonalizadas.Add(Etiquetas);
            }
            if (datosEtiquetas5 != null)
            {
                string[] r = new string[4];
                r = datosEtiquetas5.Split('|');
                EquimarFac.ServicioFel.EtiquetaPersonalizadaR Etiquetas = new EquimarFac.ServicioFel.EtiquetaPersonalizadaR();
                Etiquetas.Nombre = r[1];
                Etiquetas.Valor = r[2];
                listaEtiquetasPersonalizadas.Add(Etiquetas);
            }
            addenda.EtiquetasPersonalizadas = listaEtiquetasPersonalizadas.ToArray();
            EquimarFac.ServicioFel.DomicilioClienteR domiciliocliente = new ServicioFel.DomicilioClienteR();
            EquimarFac.ServicioFel.DomicilioClienteR domiciliocliente1 = new ServicioFel.DomicilioClienteR();

            if (calleReceptor == "")
            {
                domiciliocliente.Calle = null;
            }
            else
            {
                domiciliocliente.Calle = calleReceptor;
            }

            if (noExteriorReceptor == "")
            {
                domiciliocliente.NumeroExterior = null;
            }
            else
            {
                domiciliocliente.NumeroExterior = noExteriorReceptor;
            }
            if (noInteriorReceptor == "")
            {
                domiciliocliente.NumeroInterior = null;
            }
            else
            {
                domiciliocliente.NumeroInterior = noInteriorReceptor;
            }
            if (coloniaReceptor == "")
            {
                domiciliocliente.Colonia = null;
            }
            else
            {
                domiciliocliente.Colonia = coloniaReceptor;
            }
            if (coloniaReceptor == "")
            {
                domiciliocliente.Colonia = null;
            }
            else
            {
                domiciliocliente.Colonia = coloniaReceptor;
            }
            if (localidadReceptor == "")
            {
                domiciliocliente.Localidad = null;
            }
            else
            {
                domiciliocliente.Localidad = localidadReceptor;
            }
            if (municipioReceptor == "")
            {
                domiciliocliente.Municipio = null;
            }
            else
            {
                domiciliocliente.Municipio = municipioReceptor;
            }
            if (estadoReceptor == "")
            {
                domiciliocliente.Estado = null;
            }
            else
            {
                domiciliocliente.Estado = estadoReceptor;
            }
            if (paisReceptor == "México​")
            {
                domiciliocliente.Pais = "MEX";
            }
            if (paisReceptor == "Afganistán​")
            {
                domiciliocliente.Pais = "AFG";
            }
            if (paisReceptor == "Islas Åland​")
            {
                domiciliocliente.Pais = "ALA";
            }
            if (paisReceptor == "Albania")
            {
                domiciliocliente.Pais = "ALB";
            }
            if (paisReceptor == "Alemania")
            {
                domiciliocliente.Pais = "DEU";
            }
            if (paisReceptor == "Andorra")
            {
                domiciliocliente.Pais = "AND";
            }
            if (paisReceptor == "Angola")
            {
                domiciliocliente.Pais = "AGO";
            }
            if (paisReceptor == "Anguila")
            {
                domiciliocliente.Pais = "AIA";
            }
            if (paisReceptor == "Antártida")
            {
                domiciliocliente.Pais = "Antártida";
            }
            if (paisReceptor == "Antigua y Barbuda")
            {
                domiciliocliente.Pais = "ATG";
            }
            if (paisReceptor == "Arabia Saudita")
            {
                domiciliocliente.Pais = "SAU";
            }
            if (paisReceptor == "Argelia")
            {
                domiciliocliente.Pais = "DZA";
            }
            if (paisReceptor == "Argentina")
            {
                domiciliocliente.Pais = "ARG";
            }
            if (paisReceptor == "Armenia")
            {
                domiciliocliente.Pais = "ARM";
            }
            if (paisReceptor == "Aruba")
            {
                domiciliocliente.Pais = "ABW";
            }
            if (paisReceptor == "Australia")
            {
                domiciliocliente.Pais = "AUS";
            }
            if (paisReceptor == "Austria")
            {
                domiciliocliente.Pais = "AUT";
            }
            if (paisReceptor == "Azerbaiyán")
            {
                domiciliocliente.Pais = "AZE";
            }
            if (paisReceptor == "Bahamas (las)")
            {
                domiciliocliente.Pais = "BHS";
            }
            if (paisReceptor == "Bangladés")
            {
                domiciliocliente.Pais = "BGD";
            }
            if (paisReceptor == "Barbados")
            {
                domiciliocliente.Pais = "BRB";
            }
            if (paisReceptor == "Baréin ")
            {
                domiciliocliente.Pais = "BHR";
            }
            if (paisReceptor == "Bélgica")
            {
                domiciliocliente.Pais = "BEL";
            }
            if (paisReceptor == "Belice")
            {
                domiciliocliente.Pais = "BLZ";
            }
            if (paisReceptor == "Benín")
            {
                domiciliocliente.Pais = "BEN";
            }
            if (paisReceptor == "Bermudas")
            {
                domiciliocliente.Pais = "BMU";
            }
            if (paisReceptor == "Bielorrusia")
            {
                domiciliocliente.Pais = "BLR";
            }
            if (paisReceptor == "Myanmar")
            {
                domiciliocliente.Pais = "MMR";
            }
            if (paisReceptor == "Bolivia, Estado Plurinacional de")
            {
                domiciliocliente.Pais = "BOL";
            }
            if (paisReceptor == "Bosnia y Herzegovina")
            {
                domiciliocliente.Pais = "BIH";
            }
            if (paisReceptor == "Botsuana")
            {
                domiciliocliente.Pais = "BWA";
            }
            if (paisReceptor == "Brasil")
            {
                domiciliocliente.Pais = "BRA";
            }
            if (paisReceptor == "Brunéi Darussalam")
            {
                domiciliocliente.Pais = "BRN";
            }
            if (paisReceptor == "Bulgaria")
            {
                domiciliocliente.Pais = "BGR";
            }
            if (paisReceptor == "Burkina Faso")
            {
                domiciliocliente.Pais = "BFA";
            }
            if (paisReceptor == "Burundi")
            {
                domiciliocliente.Pais = "BDI";
            }
            if (paisReceptor == "Bután")
            {
                domiciliocliente.Pais = "BTN";
            }
            if (paisReceptor == "Cabo Verde")
            {
                domiciliocliente.Pais = "CPV";
            }
            if (paisReceptor == "Camboya")
            {
                domiciliocliente.Pais = "KHM";
            }
            if (paisReceptor == "Camerún")
            {
                domiciliocliente.Pais = "CMR";
            }
            if (paisReceptor == "Canadá")
            {
                domiciliocliente.Pais = "CAN";
            }
            if (paisReceptor == "Catar")
            {
                domiciliocliente.Pais = "QAT";
            }
            if (paisReceptor == "Cabo Verde")
            {
                domiciliocliente.Pais = "BES";
            }
            if (paisReceptor == "Chad")
            {
                domiciliocliente.Pais = "TCD";
            }
            if (paisReceptor == "Chile")
            {
                domiciliocliente.Pais = "CHL";
            }
            if (paisReceptor == "China")
            {
                domiciliocliente.Pais = "CHN";
            }
            if (paisReceptor == "Chipre")
            {
                domiciliocliente.Pais = "CYP";
            }
            if (paisReceptor == "Colombia")
            {
                domiciliocliente.Pais = "COL";
            }
            if (paisReceptor == "Comoras")
            {
                domiciliocliente.Pais = "ABW";
            }
            if (paisReceptor == "Corea (la República Democrática Popular de)")
            {
                domiciliocliente.Pais = "PRK";
            }
            if (paisReceptor == "Corea (la República de)")
            {
                domiciliocliente.Pais = "KOR";
            }
            if (paisReceptor == "Côte d'Ivoire")
            {
                domiciliocliente.Pais = "CIV";
            }
            if (paisReceptor == "Costa Rica")
            {
                domiciliocliente.Pais = "CRI";
            }
            if (paisReceptor == "Croacia")
            {
                domiciliocliente.Pais = "HRV";
            }
            if (paisReceptor == "Cuba")
            {
                domiciliocliente.Pais = "CUB";
            }
            if (paisReceptor == "Curaçao")
            {
                domiciliocliente.Pais = "CUW";
            }
            if (paisReceptor == "Dinamarca")
            {
                domiciliocliente.Pais = "DNK";
            }
            if (paisReceptor == "Dominica")
            {
                domiciliocliente.Pais = "DMA";
            }
            if (paisReceptor == "Ecuador")
            {
                domiciliocliente.Pais = "ECU";
            }
            if (paisReceptor == "Egipto")
            {
                domiciliocliente.Pais = "EGY";
            }
            if (paisReceptor == "El Salvador")
            {
                domiciliocliente.Pais = "SLV";
            }
            if (paisReceptor == "Emiratos Árabes Unidos (Los)")
            {
                domiciliocliente.Pais = "ARE";
            }
            if (paisReceptor == "Eritrea")
            {
                domiciliocliente.Pais = "ERI";
            }
            if (paisReceptor == "Eslovaquia")
            {
                domiciliocliente.Pais = "SVK";
            }
            if (paisReceptor == "Eslovenia")
            {
                domiciliocliente.Pais = "SVN";
            }
            if (paisReceptor == "España")
            {
                domiciliocliente.Pais = "ESP";
            }
            if (paisReceptor == "Estados Unidos (los)")
            {
                domiciliocliente.Pais = "USA";
            }
            if (paisReceptor == "Estonia")
            {
                domiciliocliente.Pais = "EST";
            }
            if (paisReceptor == "Etiopía")
            {
                domiciliocliente.Pais = "ETH";
            }
            if (paisReceptor == "Filipinas (las)")
            {
                domiciliocliente.Pais = "PHL";
            }
            if (paisReceptor == "Finlandia")
            {
                domiciliocliente.Pais = "FIN";
            }
            if (paisReceptor == "Aruba")
            {
                domiciliocliente.Pais = "FJI";
            }
            if (paisReceptor == "Francia")
            {
                domiciliocliente.Pais = "FRA";
            }
            if (paisReceptor == "Gabón")
            {
                domiciliocliente.Pais = "GAB";
            }
            if (paisReceptor == "Gambia (La)")
            {
                domiciliocliente.Pais = "GMB";
            }
            if (paisReceptor == "Georgia")
            {
                domiciliocliente.Pais = "GEO";
            }
            if (paisReceptor == "Ghana")
            {
                domiciliocliente.Pais = "GHA";
            }
            if (paisReceptor == "Gibraltar")
            {
                domiciliocliente.Pais = "GIB";
            }
            if (paisReceptor == "Granada")
            {
                domiciliocliente.Pais = "GRD";
            }
            if (paisReceptor == "Grecia")
            {
                domiciliocliente.Pais = "GRC";
            }
            if (paisReceptor == "Groenlandia")
            {
                domiciliocliente.Pais = "GRL";
            }
            if (paisReceptor == "Guadalupe")
            {
                domiciliocliente.Pais = "GLP";
            }
            if (paisReceptor == "Guam")
            {
                domiciliocliente.Pais = "GUM";
            }
            if (paisReceptor == "Guatemala")
            {
                domiciliocliente.Pais = "GTM";
            }
            if (paisReceptor == "Guayana Francesa")
            {
                domiciliocliente.Pais = "GUF";
            }
            if (paisReceptor == "Guernsey")
            {
                domiciliocliente.Pais = "GGY";
            }
            if (paisReceptor == "Guinea")
            {
                domiciliocliente.Pais = "GIN";
            }
            if (paisReceptor == "Guinea-Bisáu")
            {
                domiciliocliente.Pais = "GNB";
            }
            if (paisReceptor == "Guinea Ecuatorial")
            {
                domiciliocliente.Pais = "GNQ";
            }
            if (paisReceptor == "Guyana")
            {
                domiciliocliente.Pais = "GUY";
            }
            if (paisReceptor == "Haití")
            {
                domiciliocliente.Pais = "HTI";
            }
            if (paisReceptor == "Honduras")
            {
                domiciliocliente.Pais = "HND";
            }
            if (paisReceptor == "Hong Kong")
            {
                domiciliocliente.Pais = "HKG";
            }
            if (paisReceptor == "Hungría")
            {
                domiciliocliente.Pais = "HUN";
            }
            if (paisReceptor == "India")
            {
                domiciliocliente.Pais = "IND";
            }
            if (paisReceptor == "Indonesia")
            {
                domiciliocliente.Pais = "IDN";
            }
            if (paisReceptor == "Irak")
            {
                domiciliocliente.Pais = "IRQ";
            }
            if (paisReceptor == "Irán (la República Islámica de)")
            {
                domiciliocliente.Pais = "IRN";
            }
            if (paisReceptor == "Irlanda")
            {
                domiciliocliente.Pais = "IRL";
            }
            if (paisReceptor == "Isla Bouvet")
            {
                domiciliocliente.Pais = "BVT";
            }
            if (paisReceptor == "Isla de Man")
            {
                domiciliocliente.Pais = "IMN";
            }
            if (paisReceptor == "Isla de Navidad")
            {
                domiciliocliente.Pais = "CXR";
            }
            if (paisReceptor == "Isla Norfolk")
            {
                domiciliocliente.Pais = "NFK";
            }
            if (paisReceptor == "Islandia")
            {
                domiciliocliente.Pais = "ISL";
            }
            if (paisReceptor == "Islas Caimán (las)")
            {
                domiciliocliente.Pais = "CYM";
            }
            if (paisReceptor == "Islas Cocos (Keeling)")
            {
                domiciliocliente.Pais = "CCK";
            }
            if (paisReceptor == "Islas Cook (las)")
            {
                domiciliocliente.Pais = "COK";
            }
            if (paisReceptor == "Islas Feroe (las)")
            {
                domiciliocliente.Pais = "FRO";
            }
            if (paisReceptor == "Georgia del sur y las islas sandwich del sur")
            {
                domiciliocliente.Pais = "SGS";
            }
            if (paisReceptor == "Isla Heard e Islas McDonald")
            {
                domiciliocliente.Pais = "HMD";
            }
            if (paisReceptor == "Islas Malvinas [Falkland] (las)")
            {
                domiciliocliente.Pais = "FLK";
            }
            if (paisReceptor == "Islas Marianas del Norte (las)")
            {
                domiciliocliente.Pais = "MNP";
            }
            if (paisReceptor == "Islas Marshall (las)")
            {
                domiciliocliente.Pais = "MHL";
            }
            if (paisReceptor == "Pitcairn")
            {
                domiciliocliente.Pais = "PCN";
            }
            if (paisReceptor == "Islas Salomón (las)")
            {
                domiciliocliente.Pais = "SLB";
            }
            if (paisReceptor == "Islas Turcas y Caicos (las)")
            {
                domiciliocliente.Pais = "TCA";
            }
            if (paisReceptor == "Islas de Ultramar Menores de Estados Unidos (las)")
            {
                domiciliocliente.Pais = "UMI";
            }
            if (paisReceptor == "Islas Vírgenes (Británicas)")
            {
                domiciliocliente.Pais = "VGB";
            }
            if (paisReceptor == "Islas Vírgenes (EE.UU.)")
            {
                domiciliocliente.Pais = "VIR";
            }
            if (paisReceptor == "Israel")
            {
                domiciliocliente.Pais = "ISR";
            }
            if (paisReceptor == "Italia")
            {
                domiciliocliente.Pais = "ITA";
            }
            if (paisReceptor == "Jamaica")
            {
                domiciliocliente.Pais = "JAM";
            }
            if (paisReceptor == "Japón")
            {
                domiciliocliente.Pais = "JPN";
            }
            if (paisReceptor == "Jersey")
            {
                domiciliocliente.Pais = "JEY";
            }
            if (paisReceptor == "Jordania")
            {
                domiciliocliente.Pais = "JOR";
            }
            if (paisReceptor == "Kazajistán")
            {
                domiciliocliente.Pais = "KAZ";
            }
            if (paisReceptor == "Kenia")
            {
                domiciliocliente.Pais = "KEN";
            }
            if (paisReceptor == "Kirguistán")
            {
                domiciliocliente.Pais = "KGZ";
            }
            if (paisReceptor == "Kiribati")
            {
                domiciliocliente.Pais = "KIR";
            }
            if (paisReceptor == "Kuwait")
            {
                domiciliocliente.Pais = "KWT";
            }
            if (paisReceptor == "Lao, (la) República Democrática Popular")
            {
                domiciliocliente.Pais = "LAO";
            }
            if (paisReceptor == "Lesoto")
            {
                domiciliocliente.Pais = "LSO";
            }
            if (paisReceptor == "Letonia")
            {
                domiciliocliente.Pais = "LVA";
            }
            if (paisReceptor == "Líbano")
            {
                domiciliocliente.Pais = "LBN";
            }
            if (paisReceptor == "Liberia")
            {
                domiciliocliente.Pais = "LBR";
            }
            if (paisReceptor == "Libia")
            {
                domiciliocliente.Pais = "LBY";
            }
            if (paisReceptor == "Liechtenstein")
            {
                domiciliocliente.Pais = "LIE";
            }
            if (paisReceptor == "Lituania")
            {
                domiciliocliente.Pais = "LTU";
            }
            if (paisReceptor == "Luxemburgo")
            {
                domiciliocliente.Pais = "LUX";
            }
            if (paisReceptor == "Macao")
            {
                domiciliocliente.Pais = "MAC";
            }
            if (paisReceptor == "Madagascar")
            {
                domiciliocliente.Pais = "MDG";
            }
            if (paisReceptor == "Malasia")
            {
                domiciliocliente.Pais = "MYS";
            }
            if (paisReceptor == "Malaui")
            {
                domiciliocliente.Pais = "MWI";
            }
            if (paisReceptor == "Maldivas")
            {
                domiciliocliente.Pais = "MDV";
            }
            if (paisReceptor == "Malí")
            {
                domiciliocliente.Pais = "MLI";
            }
            if (paisReceptor == "Malta")
            {
                domiciliocliente.Pais = "MLT";
            }
            if (paisReceptor == "Marruecos")
            {
                domiciliocliente.Pais = "MAR";
            }
            if (paisReceptor == "Martinica")
            {
                domiciliocliente.Pais = "MTQ";
            }
            if (paisReceptor == "Mauricio")
            {
                domiciliocliente.Pais = "MUS";
            }
            if (paisReceptor == "Mauritania")
            {
                domiciliocliente.Pais = "MRT";
            }
            if (paisReceptor == "Mayotte")
            {
                domiciliocliente.Pais = "MYT";
            }
            if (paisReceptor == "Micronesia (los Estados Federados de)")
            {
                domiciliocliente.Pais = "FSM";
            }
            if (paisReceptor == "Moldavia (la República de)")
            {
                domiciliocliente.Pais = "MDA";
            }
            if (paisReceptor == "Mónaco")
            {
                domiciliocliente.Pais = "MCO";
            }
            if (paisReceptor == "Mongolia")
            {
                domiciliocliente.Pais = "MNG";
            }
            if (paisReceptor == "Montenegro")
            {
                domiciliocliente.Pais = "MNE";
            }
            if (paisReceptor == "Montserrat")
            {
                domiciliocliente.Pais = "MSR";
            }
            if (paisReceptor == "Mozambique")
            {
                domiciliocliente.Pais = "MOZ";
            }
            if (paisReceptor == "Namibia")
            {
                domiciliocliente.Pais = "NAM";
            }
            if (paisReceptor == "Nauru")
            {
                domiciliocliente.Pais = "NRU";
            }
            if (paisReceptor == "Nepal")
            {
                domiciliocliente.Pais = "NPL";
            }
            if (paisReceptor == "Nicaragua")
            {
                domiciliocliente.Pais = "NIC";
            }
            if (paisReceptor == "Níger (el)")
            {
                domiciliocliente.Pais = "NER";
            }
            if (paisReceptor == "Nigeria")
            {
                domiciliocliente.Pais = "NGA";
            }
            if (paisReceptor == "Niue")
            {
                domiciliocliente.Pais = "NIU";
            }
            if (paisReceptor == "Noruega")
            {
                domiciliocliente.Pais = "NOR";
            }
            if (paisReceptor == "Nueva Caledonia")
            {
                domiciliocliente.Pais = "NCL";
            }
            if (paisReceptor == "Nueva Zelanda")
            {
                domiciliocliente.Pais = "NZL";
            }
            if (paisReceptor == "Omán")
            {
                domiciliocliente.Pais = "OMN";
            }
            if (paisReceptor == "Países Bajos (los)")
            {
                domiciliocliente.Pais = "NLD";
            }
            if (paisReceptor == "Pakistán")
            {
                domiciliocliente.Pais = "PAK";
            }
            if (paisReceptor == "Palaos")
            {
                domiciliocliente.Pais = "PLW";
            }
            if (paisReceptor == "Palestina, Estado de")
            {
                domiciliocliente.Pais = "PSE";
            }
            if (paisReceptor == "Panamá")
            {
                domiciliocliente.Pais = "PAN";
            }
            if (paisReceptor == "Papúa Nueva Guinea")
            {
                domiciliocliente.Pais = "PNG";
            }
            if (paisReceptor == "Paraguay")
            {
                domiciliocliente.Pais = "PRY";
            }
            if (paisReceptor == "Perú")
            {
                domiciliocliente.Pais = "PER";
            }
            if (paisReceptor == "Polinesia Francesa")
            {
                domiciliocliente.Pais = "PYF";
            }
            if (paisReceptor == "Polonia")
            {
                domiciliocliente.Pais = "POL";
            }
            if (paisReceptor == "Portugal")
            {
                domiciliocliente.Pais = "PRT";
            }
            if (paisReceptor == "Puerto Rico")
            {
                domiciliocliente.Pais = "PRI";
            }
            if (paisReceptor == "Reino Unido (el)")
            {
                domiciliocliente.Pais = "GBR";
            }
            if (paisReceptor == "República Centroafricana (la)")
            {
                domiciliocliente.Pais = "CAF";
            }
            if (paisReceptor == "República Checa (la)")
            {
                domiciliocliente.Pais = "CZE";
            }
            if (paisReceptor == "Macedonia (la antigua República Yugoslava de)")
            {
                domiciliocliente.Pais = "MKD";
            }
            if (paisReceptor == "Congo")
            {
                domiciliocliente.Pais = "COG";
            }
            if (paisReceptor == "Congo (la República Democrática del)")
            {
                domiciliocliente.Pais = "COD";
            }
            if (paisReceptor == "República Dominicana (la)")
            {
                domiciliocliente.Pais = "DOM";
            }
            if (paisReceptor == "Reunión")
            {
                domiciliocliente.Pais = "REU";
            }
            if (paisReceptor == "Ruanda")
            {
                domiciliocliente.Pais = "RWA";
            }
            if (paisReceptor == "Rumania")
            {
                domiciliocliente.Pais = "ROU";
            }
            if (paisReceptor == "Rusia, (la) Federación de")
            {
                domiciliocliente.Pais = "RUS";
            }
            if (paisReceptor == "Sahara Occidental")
            {
                domiciliocliente.Pais = "ESH";
            }
            if (paisReceptor == "Samoa")
            {
                domiciliocliente.Pais = "WSM";
            }
            if (paisReceptor == "Samoa Americana")
            {
                domiciliocliente.Pais = "ASM";
            }
            if (paisReceptor == "San Bartolomé")
            {
                domiciliocliente.Pais = "BLM";
            }
            if (paisReceptor == "San Cristóbal y Nieves")
            {
                domiciliocliente.Pais = "KNA";
            }
            if (paisReceptor == "San Marino")
            {
                domiciliocliente.Pais = "SMR";
            }
            if (paisReceptor == "San Martín (parte francesa)")
            {
                domiciliocliente.Pais = "MAF";
            }
            if (paisReceptor == "San Pedro y Miquelón")
            {
                domiciliocliente.Pais = "SPM";
            }
            if (paisReceptor == "San Vicente y las Granadinas")
            {
                domiciliocliente.Pais = "VCT";
            }
            if (paisReceptor == "Santa Helena, Ascensión y Tristán de Acuña")
            {
                domiciliocliente.Pais = "SHN";
            }
            if (paisReceptor == "Santa Lucía")
            {
                domiciliocliente.Pais = "LCA";
            }
            if (paisReceptor == "Santo Tomé y Príncipe")
            {
                domiciliocliente.Pais = "STP";
            }
            if (paisReceptor == "Senegal")
            {
                domiciliocliente.Pais = "SEN";
            }
            if (paisReceptor == "Serbia")
            {
                domiciliocliente.Pais = "SRB";
            }
            if (paisReceptor == "Seychelles")
            {
                domiciliocliente.Pais = "SYC";
            }
            if (paisReceptor == "Sierra leona")
            {
                domiciliocliente.Pais = "SLE";
            }
            if (paisReceptor == "Singapur")
            {
                domiciliocliente.Pais = "SGP";
            }
            if (paisReceptor == "Sint Maarten (parte holandesa)")
            {
                domiciliocliente.Pais = "SXM";
            }
            if (paisReceptor == "Siria, (la) República Árabe")
            {
                domiciliocliente.Pais = "SYR";
            }
            if (paisReceptor == "Somalia")
            {
                domiciliocliente.Pais = "SOM";
            }
            if (paisReceptor == "Sri Lanka")
            {
                domiciliocliente.Pais = "LKA";
            }
            if (paisReceptor == "Suazilandia")
            {
                domiciliocliente.Pais = "SWZ";
            }
            if (paisReceptor == "Sudáfrica")
            {
                domiciliocliente.Pais = "ZAF";
            }
            if (paisReceptor == "Sudán (el)")
            {
                domiciliocliente.Pais = "SDN";
            }
            if (paisReceptor == "Sudán del Sur")
            {
                domiciliocliente.Pais = "SSD";
            }
            if (paisReceptor == "Suecia")
            {
                domiciliocliente.Pais = "SWE";
            }
            if (paisReceptor == "Suiza")
            {
                domiciliocliente.Pais = "CHE";
            }
            if (paisReceptor == "Surinam")
            {
                domiciliocliente.Pais = "SUR";
            }
            if (paisReceptor == "Svalbard y Jan Mayen")
            {
                domiciliocliente.Pais = "SJM";
            }
            if (paisReceptor == "Tailandia")
            {
                domiciliocliente.Pais = "THA";
            }
            if (paisReceptor == "Taiwán (Provincia de China)")
            {
                domiciliocliente.Pais = "TWN";
            }
            if (paisReceptor == "Tanzania, República Unida de")
            {
                domiciliocliente.Pais = "TZA";
            }
            if (paisReceptor == "Tayikistán")
            {
                domiciliocliente.Pais = "TJK";
            }
            if (paisReceptor == "Territorio Británico del Océano Índico (el)")
            {
                domiciliocliente.Pais = "IOT";
            }
            if (paisReceptor == "Territorios Australes Franceses (los)")
            {
                domiciliocliente.Pais = "ATF";
            }
            if (paisReceptor == "Timor-Leste")
            {
                domiciliocliente.Pais = "TLS";
            }
            if (paisReceptor == "Togo")
            {
                domiciliocliente.Pais = "TGO";
            }
            if (paisReceptor == "Tokelau")
            {
                domiciliocliente.Pais = "TKL";
            }
            if (paisReceptor == "Tonga")
            {
                domiciliocliente.Pais = "TON";
            }
            if (paisReceptor == "Trinidad y Tobago")
            {
                domiciliocliente.Pais = "TTO";
            }
            if (paisReceptor == "Túnez")
            {
                domiciliocliente.Pais = "TUN";
            }
            if (paisReceptor == "Turkmenistán")
            {
                domiciliocliente.Pais = "TKM";
            }
            if (paisReceptor == "Turquía")
            {
                domiciliocliente.Pais = "TUR";
            }
            if (paisReceptor == "Tuvalu")
            {
                domiciliocliente.Pais = "TUV";
            }
            if (paisReceptor == "Ucrania")
            {
                domiciliocliente.Pais = "UKR";
            }
            if (paisReceptor == "Uganda")
            {
                domiciliocliente.Pais = "UGA";
            }
            if (paisReceptor == "Uruguay")
            {
                domiciliocliente.Pais = "URY";
            }
            if (paisReceptor == "Uzbekistán")
            {
                domiciliocliente.Pais = "UZB";
            }
            if (paisReceptor == "Vanuatu")
            {
                domiciliocliente.Pais = "VUT";
            }
            if (paisReceptor == "Santa Sede[Estado de la Ciudad del Vaticano] (la)")
            {
                domiciliocliente.Pais = "VAT";
            }
            if (paisReceptor == "Venezuela, República Bolivariana de")
            {
                domiciliocliente.Pais = "VEN";
            }
            if (paisReceptor == "Viet Nam")
            {
                domiciliocliente.Pais = "VNM";
            }
            if (paisReceptor == "Wallis y Futuna")
            {
                domiciliocliente.Pais = "WLF";
            }
            if (paisReceptor == "Yemen")
            {
                domiciliocliente.Pais = "YEM";
            }
            if (paisReceptor == "Yibuti")
            {
                domiciliocliente.Pais = "DJI";
            }
            if (paisReceptor == "Zambia")
            {
                domiciliocliente.Pais = "ZMB";
            }
            if (paisReceptor == "Zimbabue")
            {
                domiciliocliente.Pais = "ZWE";
            }
            if (paisReceptor == "paises no declarados")
            {
                domiciliocliente.Pais = "ZZZ";
            }

            if (codigoPostalReceptor == "")
            {
                domiciliocliente.CodigoPostal = null;
            }
            else
            {
                domiciliocliente.CodigoPostal = codigoPostalReceptor;
            }
            if (codigoPostalReceptor == "")
            {
                domiciliocliente.CodigoPostal = null;
            }
            else
            {
                domiciliocliente.CodigoPostal = codigoPostalReceptor;
            }

            domiciliocliente1.Calle = "Calle 25 x 84";



            domiciliocliente1.NumeroExterior = "No. 156";



            domiciliocliente1.NumeroInterior = null;


            domiciliocliente1.Colonia = "Centro";

            domiciliocliente1.Municipio = "Progreso";

            domiciliocliente1.Estado = "Yucatán";



            domiciliocliente1.Pais = "MEX";



            domiciliocliente1.CodigoPostal = "97320";


            domiciliocliente1.Telefono = "9699 350054";
            addenda.DomicilioReceptor = domiciliocliente;
            addenda.DomicilioEmisor = domiciliocliente1;
            datosCFDI.Addenda = addenda;
            int contador = 0;
            foreach (string i in conceptos)
            {
                EquimarFac.ServicioFel.ConceptoR concepto = new ServicioFel.ConceptoR();

                EquimarFac.ServicioFel.ImpuestosConceptoR ConceptoImpuestos1 = new ServicioFel.ImpuestosConceptoR();
                List<EquimarFac.ServicioFel.TrasladoConceptoR> ListaTraslado1 = new List<EquimarFac.ServicioFel.TrasladoConceptoR>();
                EquimarFac.ServicioFel.TrasladoConceptoR traslado1 = new ServicioFel.TrasladoConceptoR();

                string[] datosConceptos = i.Split('|');
                concepto.Cantidad = Convert.ToDecimal(datosConceptos[1]);
                concepto.ClaveProdServ = datosConceptos[3];
                concepto.ClaveUnidad = datosConceptos[2];
                concepto.Descripcion = datosConceptos[4];
                concepto.ValorUnitario = Convert.ToDecimal(datosConceptos[5]);
                concepto.Importe = Convert.ToDecimal(datosConceptos[6]);

                traslado1.Base = concepto.Importe;
                traslado1.Impuesto = "002";
                traslado1.TipoFactor = "Tasa";
                traslado1.TasaOCuota = Decimal.Parse("0.160000");
                traslado1.Importe = Convert.ToDecimal(Convert.ToDouble(concepto.Importe) * .16);
                ListaTraslado1.Add(traslado1);
                ConceptoImpuestos1.Traslados = ListaTraslado1.ToArray();

                concepto.Impuestos = ConceptoImpuestos1;

                listaconceptos.Add(concepto);
                contador = contador + 1;
            }
            datosCFDI.Conceptos = listaconceptos.ToArray();
            respuesta1 = coneccion.GenerarCFDI(datosUsuario, datosCFDI);
            respuesta[3] = respuesta1.OperacionExitosa.ToString();
            respuesta[0] = respuesta1.CBB;
            respuesta[5] = respuesta1.XML.ToString();
            return respuesta;

        }
            catch
            {
                respuesta[1] = respuesta1.ErrorDetallado.ToString();
                respuesta[2] = respuesta1.ErrorGeneral.ToString();
                return respuesta;
            }
}

        public string[] GenerarFacturaBidimensional()
        {
            string[] datosUsuario = new string[3];
            string[] respuesta = new string[3];
            try
            {
                datosUsuario[0] = emisorRFC;
                // Cuenta del usuario (REQUERIDO);. Posición 1.
                datosUsuario[1] = CuentaFEL;
                // Password del usuario (REQUERIDO);. Posición 2.
                datosUsuario[2] = PasswordFEL;

                rnd.ConexionRemota32 coneccion=new EquimarFac.FelWebService.ConexionRemota32();
                
                return respuesta = coneccion.GenerarCodigoBidimensional(datosUsuario, UUID);    
            }
            catch
            {
                return respuesta;
            }
        }

        public string[] cancelacdfi()
        {
            string[] datosUsuario = new string[3];
            string[] respuesta = new string[3];
            string[] listaUUID = new string[1];
            EquimarFac.ServicioFel.Credenciales crendencial = new ServicioFel.Credenciales();
            crendencial.Password = PasswordFEL;
            crendencial.Usuario = emisorRFC;
            crendencial.Cuenta = CuentaFEL;
            listaUUID[0] = UUID;
            EquimarFac.ServicioFel.ConexionRemotaClient coneccion = new ServicioFel.ConexionRemotaClient();
            EquimarFac.ServicioFel.RespuestaCancelacionCR respu = new ServicioFel.RespuestaCancelacionCR();
            try
            {

                respu = coneccion.CancelarCFDIs(crendencial, listaUUID); 
                respuesta[0] = respu.OperacionExitosa.ToString();
                
                respuesta[2] = respu.Acuse.ToString();
                
                respuesta[1] = respu.UUIDS[0].ToString();



                return respuesta;


            }
            catch
            {
                respuesta[1] = respu.ErrorGeneral.ToString() + ' ' + respu.ErrorDetallado.ToString();
                return respuesta;
            }
        }

        public string[] enviacfdicorreo()
        {
            string[] datosUsuario = new string[3];
            string[] respuesta = new string[2];
            try
            {
                datosUsuario[0] = emisorRFC; ;
                // Cuenta del usuario (REQUERIDO);. Posición 1.
                datosUsuario[1] = CuentaFEL;
                // Password del usuario (REQUERIDO);. Posición 2.
                datosUsuario[2] = PasswordFEL;

                rnd.ConexionRemota32 coneccion = new EquimarFac.FelWebService.ConexionRemota32();

                return respuesta = coneccion.EnviarCFDI(datosUsuario, UUID, Email);
            }
            catch
            {
                return respuesta;
            }
        }


        public string[] obtenerpdf()
        {
            string[] datosUsuario = new string[3];
            string[] respuesta = new string[4];
            EquimarFac.ServicioFel.Credenciales crendencial = new ServicioFel.Credenciales();
            crendencial.Password = PasswordFEL;
            crendencial.Usuario = emisorRFC;
            crendencial.Cuenta = CuentaFEL;
            string nombreplantilla = "";
            EquimarFac.ServicioFel.ConexionRemotaClient coneccion = new ServicioFel.ConexionRemotaClient();
            EquimarFac.ServicioFel.RespuestaOperacionCR oper = new ServicioFel.RespuestaOperacionCR();
            try
            {
                oper = coneccion.ObtenerPDF(crendencial, UUID, nombreplantilla);
                respuesta[0] = oper.OperacionExitosa.ToString();
                respuesta[3] = oper.PDF.ToString();
                return respuesta;
            }
            catch
            {
                respuesta[1] = oper.ErrorGeneral.ToString();
                respuesta[2] = oper.ErrorDetallado.ToString();
                return respuesta;
            }
        }


        public string[] consultarcreditos()
        {
            string[] respue = new string[5];
            EquimarFac.ServicioFel.Credenciales crendencial = new ServicioFel.Credenciales();
            crendencial.Password = PasswordFEL;
            crendencial.Usuario = emisorRFC;
            crendencial.Cuenta = CuentaFEL;
            EquimarFac.ServicioFel.RespuestaNumeroCreditosCR respuesta = new ServicioFel.RespuestaNumeroCreditosCR();
            EquimarFac.ServicioFel.ConexionRemotaClient coneccion = new ServicioFel.ConexionRemotaClient();
            try
            {


                respuesta = coneccion.ObtenerNumerosCreditos(crendencial);
                respue[0] = respuesta.OperacionExitosa.ToString();
                respue[3] = respuesta.CreditosUsados.ToString();
                respue[4] = respuesta.CreditosRestantes.ToString();
                return respue;
            }
            catch
            {
                respue[1] = respuesta.ErrorGeneral.ToString();
                respue[2] = respuesta.ErrorDetallado.ToString();
                return respue;
            }
        }
















            
    }
}
